const fetchUsers = fetch('https://ajax.test-danit.com/api/json/users');
const fetchPosts = fetch('https://ajax.test-danit.com/api/json/posts');

Promise.all([fetchUsers, fetchPosts])
.then(data => {
    return Promise.all(data.map(d => d.json()))
}).then(([users, posts]) => {
    posts.forEach(post => {
        const user = users.find(user => post.userId === user.id);
        if (user) {
          const card = new Card(post.id, user.name, user.email, post.title, post.body);
          renderCard(card);
        }})
}).catch(error => console.log(error))

class Card {
    constructor (id, name, email, title, body) {
        this.id = id
        this.name = name
        this.email = email
        this.title = title
        this.body = body
    }
}

function renderCard (card) {
    const { id, name, email, title, body } = card;
    const cardHolder = document.querySelector('.cardholder');
    cardHolder.insertAdjacentHTML('beforeend',`
    <div class="card" data-id="${id}">
    <div class = "twitter">   
        <div class = "user_info">
            <img class = "img" src="twitter.png" alt="twitter-icon">
            <h2 class = "user_name">${name} <span class = "user_email">${email}</span></h2>
        </div>
        <div class = "post_info">
            <h4 class = "post_title">${title}</h4>
            <p class = "post_body">${body}</p>
            <button class = "post_delete" data-id="${id}">delete</button>
        </div>
    </div>`)

    const btnDelete = document.querySelector(`[data-id="${id}"]`);
    btnDelete.addEventListener('click', () => {
        deletePost(id)
    })
}

function deletePost (postId) {
    try {
        fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
            method: 'DELETE',
        })
        .then(() => {
            removePost(postId);
        });
    } catch (error) {
            console.log('Error deleting post:', error);
    }
}
function removePost(postId) {
    const card = document.querySelector(`[data-id="${postId}"]`);
    card.remove();
}